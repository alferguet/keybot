import { Message, MessageAttachment, TextChannel } from 'discord.js'
import { HELP_MESSAGE, INCORRECT_FEATURE } from './messages'
import { GlossaryManager } from './modules/glossary'

export class MessageManager {
  private readonly glossaryManager: GlossaryManager
  constructor() {
    this.glossaryManager = new GlossaryManager()
  }

  async process(message: Message): Promise<void> {
    const messageContent = message.content.trim().toLowerCase()
    const regex = /^([Kk][bB])!\s*([A-z]*)\s*(.*)/
    const parsedMessage = messageContent.match(regex)
    if (parsedMessage) {
      const answer = await this.processCommand(parsedMessage)
      await message.channel.send(answer)
    } else {
      await this.processContent(message)
    }
  }

  private async processCommand(parsedMessage: string[]): Promise<string> {
    const module = parsedMessage[2]
    const commandData = parsedMessage[3]?.split(' ')
    let answer: string

    switch (module) {
      case 'i':
      case 'info':
        answer = this.glossaryManager.getDefinition(commandData)
        break
      case 'ayuda':
      case 'help':
      case 'h':
        answer = HELP_MESSAGE
        break
      default:
        answer = INCORRECT_FEATURE
        break
    }
    return answer
  }

  private async processContent(msg: Message): Promise<void> {
    if (
      msg.content.includes(process.env.SHOWCASE) &&
      msg.attachments.size > 0 &&
      msg.guild?.available
    ) {
      const showcaseChannel = msg.guild.channels.resolve(
        process.env.SHOWCASE,
      ) as TextChannel
      await Promise.all(
        msg.attachments.map(async (each) => {
          const url = each.proxyURL
          const attachment = new MessageAttachment(url)
          console.log(`Posted showcase: ${msg.author.username} ${url}`)
          await showcaseChannel.send(
            `Publicado por: ${msg.author.toString()}`,
            attachment,
          )
          return
        }),
      )
    }
  }
}
