export const HELP_MESSAGE = `Uso del bot:\n\
	Buscar información: kb! info/i <termino a buscar>\n\
	Listar información disponible: kb! lista/l\n\
	Publicar foto: Menciona el canal #fotos en tu mensaje con la foto/l\n\
	Ayuda: kb! ayuda/a/h`
export const INCORRECT_FEATURE = `El comando no corresponde a ninguna función del bot`
